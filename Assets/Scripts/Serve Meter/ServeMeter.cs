﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServeMeter : MonoBehaviour
{
    public SpriteRenderer meterBackground;
    public Transform handle;
    public float indicatorSpeed;

    public Color missColor;
    public Color closeColor;
    public Color perfectColor;

    float handleGrowScale = 1.5f;
    float growFrames = 10;

    MeterResult result;
    bool goingUp;
    bool moving;

    Transform parent;
    Vector2 originalPosition;
    float radRatio;

    // Start is called before the first frame update
    void Start()
    {
        result = MeterResult.Miss;
        radRatio = Mathf.PI / 180;
        handle.localPosition = new Vector2(0, -meterBackground.size.y / 2);
        goingUp = true;
        moving = true;
        parent = transform.parent;
        originalPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            if (goingUp)
                handle.localPosition = new Vector2(0, handle.localPosition.y + indicatorSpeed);
            else
                handle.localPosition = new Vector2(0, handle.localPosition.y - indicatorSpeed);

            if (handle.localPosition.y >= meterBackground.size.y / 2)
            {
                handle.localPosition = new Vector2(0, meterBackground.size.y / 2);
                goingUp = false;
            }
            else if (handle.localPosition.y <= -meterBackground.size.y / 2)
            {
                handle.localPosition = new Vector2(0, -meterBackground.size.y / 2);
                goingUp = true;
            }
        }
    }

    public void SetResult(MeterResult m)
    {
        result = m;
    }

    public MeterResult Resolve()
    {
        //Animate the result and destroy this object.
        StartCoroutine(FinishUp());
        return result;
    }

    IEnumerator FinishUp()
    {
        moving = false;
        var handleSprite = handle.GetComponent<SpriteRenderer>();
        switch (result)
        {
            case MeterResult.Perfect:
                handleSprite.color = perfectColor;
                break;
            case MeterResult.Close:
                handleSprite.color = closeColor;
                break;
            default:
                handleSprite.color = missColor;
                break;
        }

        float growthInc = (handleGrowScale - handle.localScale.x) / growFrames;

        Debug.Log(result.ToString());
        while (handle.localScale.x < handleGrowScale || handle.localScale.y < handleGrowScale)
        {
            handle.localScale = new Vector2(handle.localScale.x + growthInc, handle.localScale.y + growthInc);
            yield return null;
        }
        while (handle.localScale.x > 1 || handle.localScale.y > 1)
        {
            handle.localScale = new Vector2(handle.localScale.x - growthInc, handle.localScale.y - growthInc);
            yield return null;
        }
        yield return new WaitForSeconds(1f);

        Destroy(gameObject);
    }
}

public enum MeterResult
{
    Miss, Close, Perfect
}
