﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServeMeterHandle : MonoBehaviour
{
    public ServeMeter meter;
    public GameObject closeZone;
    public GameObject perfectZone;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == perfectZone)
            meter.SetResult(MeterResult.Perfect);
        else if (collision.gameObject == closeZone)
            meter.SetResult(MeterResult.Close);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == perfectZone)
            meter.SetResult(MeterResult.Close);
        else if (collision.gameObject == closeZone)
            meter.SetResult(MeterResult.Miss);
    }
}
