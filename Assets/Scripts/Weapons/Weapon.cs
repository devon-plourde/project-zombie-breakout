﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public GameObject SwingGraphic;
    public int swingActiveFrames;
    public float swingFrequency;
    public float velocityModifier;

    public float pushDistance;

    protected Collider2D SwingCollider;
    protected SpriteRenderer SwingSprite;
    protected bool swinging = false;

    // Start is called before the first frame update
    void Start()
    {
        SwingSprite = SwingGraphic.GetComponent<SpriteRenderer>();
        SwingCollider = SwingGraphic.GetComponent<Collider2D>();
    }

    //protected abstract IEnumerator SwingWeapon();
    public abstract void Fire();
    public abstract void Swing();

    private void HitBall(GameObject ball)
    {
        var z = transform.parent.eulerAngles.z;
        var ballBody = ball.GetComponent<Rigidbody2D>();
        var ballVelocityMagnitude = ballBody.velocity.magnitude;
        var ballPos = ball.transform.position;
        var angle = z * (Mathf.PI / 180);
        ballBody.velocity = new Vector2(Mathf.Cos(angle) * ballVelocityMagnitude, Mathf.Sin(angle) * ballVelocityMagnitude) * velocityModifier;
    }

    private void HitEnemy(GameObject enemy)
    {
        var localEnemyPosition = enemy.transform.position - transform.parent.position;
        var radAngle = Mathf.Atan2(localEnemyPosition.y, localEnemyPosition.x);
        var moveDistance =  pushDistance - localEnemyPosition.magnitude;
        var movement = new Vector2(Mathf.Cos(radAngle) * moveDistance, Mathf.Sin(radAngle) * moveDistance);
        enemy.transform.position = new Vector2(enemy.transform.position.x + movement.x, enemy.transform.position.y + movement.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var collisionObject = collision.gameObject;
        if (collisionObject.tag == TagManager.Ball)
        {
            HitBall(collisionObject);
        }
        if (collisionObject.tag == TagManager.Zombie)
        {
            HitEnemy(collisionObject);
        }
    }
}
