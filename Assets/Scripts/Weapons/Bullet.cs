﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float movementSpeed;
    Rigidbody2D rigid;

    // Start is called before the first frame update
    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var radAngle = Mathf.PI / 180 * transform.eulerAngles.z;
        var movement = new Vector2(Mathf.Cos(radAngle), Mathf.Sin(radAngle)) * movementSpeed * Time.fixedDeltaTime;
        rigid.MovePosition(new Vector2(transform.position.x + movement.x, transform.position.y + movement.y));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
    }
}
