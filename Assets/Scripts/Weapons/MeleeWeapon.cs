﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    public override void Fire()
    {
        Swing();
    }

    public override void Swing()
    {
        if (!swinging)
            StartCoroutine(SwingWeapon());
    }

    protected IEnumerator SwingWeapon()
    {
        swinging = true;

        //Use Unity's rotate methods to flip the bat around the player... idiot.
        transform.localEulerAngles = new Vector3(0, 0, transform.localEulerAngles.z + 180);
        transform.localPosition = new Vector2(transform.localPosition.x, -transform.localPosition.y);
        SwingSprite.transform.localEulerAngles = new Vector3(0, 0, SwingSprite.transform.localEulerAngles.z + 180);
        SwingGraphic.SetActive(true);
        SwingCollider.enabled = true;

        //Check to see if anything got hit by the bat.
        for (int i = 0; i < swingActiveFrames; ++i)
        {
            yield return null;
        }

        SwingCollider.enabled = false;

        yield return new WaitForSeconds(swingFrequency);

        SwingGraphic.SetActive(false);
        SwingSprite.flipX = !SwingSprite.flipX;

        swinging = false;
    }
}
