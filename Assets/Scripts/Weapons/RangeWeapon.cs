﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWeapon : Weapon
{
    public Transform Bullet;
    public Transform Muzzle;

    public Transform BulletCasing;
    public Transform Ejector;
    public float accuracy;
    public float firingSpeed;

    bool cycling = false;

    public override void Swing()
    {
        if (!swinging)
        {
            StartCoroutine(SwingWeapon());
        }
    }

    protected IEnumerator SwingWeapon()
    {
        swinging = true;

        //Use Unity's rotate methods to flip the bat around the player... idiot.
        SwingGraphic.SetActive(true);
        SwingCollider.enabled = true;

        //Check to see if anything got hit by the bat.
        for (int i = 0; i < swingActiveFrames; ++i)
        {
            yield return null;
        }

        SwingCollider.enabled = false;

        yield return new WaitForSeconds(swingFrequency);

        SwingGraphic.SetActive(false);
        SwingSprite.flipX = !SwingSprite.flipX;

        swinging = false;
    }

    public override void Fire()
    {
        if (!cycling && !swinging)
        {
            var aimError = Random.Range(-accuracy, accuracy);
            var aimAngle = new Vector3(0, 0, Muzzle.eulerAngles.z + aimError);
            Instantiate(Bullet, Muzzle.position, Quaternion.Euler(aimAngle), null);
            Instantiate(BulletCasing, Ejector.position, Ejector.rotation, null);

            StartCoroutine(CycleWeapon());
        }
    }

    IEnumerator CycleWeapon()
    {
        cycling = true;
        yield return new WaitForSeconds(firingSpeed);
        cycling = false;
    }
}
