﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float initialVelocity;

    Rigidbody2D rigid;
    Vector2 currentVelocity;

    // Start is called before the first frame update
    void Start()
    {
        var radAngle = Mathf.PI / 180 * transform.eulerAngles.z;
        rigid = GetComponent<Rigidbody2D>();
        rigid.velocity = new Vector2(Mathf.Cos(radAngle) * initialVelocity, Mathf.Sin(radAngle) * initialVelocity);
        currentVelocity = rigid.velocity;
    }

    private void FixedUpdate()
    {
        currentVelocity = rigid.velocity;
        //Debug.Log(currentVelocity.magnitude);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 reflection = Vector2.Reflect(currentVelocity, collision.contacts[0].normal);
        rigid.velocity = reflection;
        currentVelocity = rigid.velocity;

        if(collision.gameObject.tag == TagManager.Zombie)
        {
            Destroy(collision.gameObject);
        }
    }

    private void OnDestroy()
    {
        EventManager.TriggerEvent(EventNames.BallDestroyed);
    }
}
