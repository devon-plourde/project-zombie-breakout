﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grave : MonoBehaviour
{
    public Transform zombiePrefab;
    public float minTimer;
    public float maxTimer;
    public int numberOfHits;

    float timer;

    // Start is called before the first frame update
    void Start()
    {
        EventManager.StartListening(EventNames.BallDestroyed,
            () => SpawnZombie());
        StartCoroutine(SpawningZombies());
    }

    float SetNewTimer()
    {
        var newTimer = 0f;
        if (minTimer == maxTimer)
            newTimer = minTimer;
        else
        {
            newTimer = Random.Range(minTimer, maxTimer);
        }
        return newTimer;
    }

    IEnumerator SpawningZombies()
    {
        timer = SetNewTimer();
        while (true)
        {
            yield return new WaitForSeconds(timer);
            SpawnZombie();
        }
    }

    void SpawnZombie()
    {
        Instantiate(zombiePrefab, transform.position, Quaternion.Euler(0, 0, -90), null);
        StopCoroutine(SpawningZombies());
        StartCoroutine(SpawningZombies());
    }

    void TakeDamage()
    {
        --numberOfHits;
        if(numberOfHits <= 0)
        {
            BreakUp();
        }
        
        //Crack up to reflect damage.
        //Play an animation for getting hit.
    }

    void BreakUp()
    {
        EventManager.StopListening(EventNames.BallDestroyed,
            () => SpawnZombie());
        StartCoroutine(SpawningZombies());

        //Play animation for being destroyed.

        StopCoroutine(SpawningZombies());
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == TagManager.Ball)
        {
            TakeDamage();
        }
    }
}
