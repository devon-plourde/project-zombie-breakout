﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Camera mainCam;
    public Weapon weapon;
    public float moveSpeed;
    public Transform ball;
    public Vector2 ballSpawnPosition;
    public Transform serveMeter;
    public Vector2 serveMeterPosition;

    Rigidbody2D rigid;
    ServeMeter meter;
    Transform meterTransform;
    
    // Start is called before the first frame update
    void Start()
    {
        if (mainCam == null)
            mainCam = GameObject.FindGameObjectWithTag(TagManager.MainCamera).GetComponent<Camera>();

        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        var mousePosition = mainCam.ScreenToWorldPoint(Input.mousePosition);
        var radAngle = Mathf.Atan2(mousePosition.y - transform.position.y, mousePosition.x - transform.position.x);
        transform.rotation = Quaternion.Euler(new Vector3(0,0, radAngle / (Mathf.PI / 180f)));

        rigid.velocity = new Vector2(Input.GetAxis("Horizontal") * moveSpeed, 0);

        //rigid.velocity = new Vector2(rigid.velocity.x, Input.GetAxis("Vertical") * moveSpeed);

        if(meterTransform != null)
            meterTransform.position = new Vector2(transform.position.x + serveMeterPosition.x, transform.position.y + serveMeterPosition.y);

        if (Input.GetMouseButtonDown(0))
        {
            if (isBallPresent())
            {
                weapon.Swing();
            }
            else
            {
                //if(meter == null)
                //{
                //    //Lock movement until the ball is served.

                //    var ballPosition = new Vector2(transform.position.x + serveMeterPosition.x, transform.position.y + serveMeterPosition.y);
                //    var m = Instantiate(serveMeter, ballPosition, Quaternion.identity, null);
                //    meterTransform = m;
                //    meter = m.GetComponent<ServeMeter>();
                //}
                //else
                //{
                    var ballPosition = new Vector2(transform.position.x + ballSpawnPosition.x, transform.position.y + ballSpawnPosition.y);
                    //var result = meter.Resolve();
                    Instantiate(ball, ballPosition, Quaternion.Euler(0, 0, transform.eulerAngles.z), null);
                    meter = null;
                //}
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            //Use alternate weapon attack.
            weapon.Fire();
        }
    }

    bool isBallPresent()
    {
        var balls = GameObject.FindGameObjectsWithTag(TagManager.Ball);
        return balls.Length > 0;
    }
}
