﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraBorder : MonoBehaviour
{
    public BoxCollider2D top;
    public BoxCollider2D bottom;
    public BoxCollider2D left;
    public BoxCollider2D right;

    Camera cam;
    float camWidth;
    float camHeight;

    private void Awake()
    {
        cam = this.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        camHeight = cam.orthographicSize * 2;
        camWidth = camHeight * cam.aspect;
        if(top != null) {
            top.size = new Vector2(camWidth, top.size.y);
            top.transform.position = new Vector2(transform.position.x, transform.position.y + (camHeight / 2) + (top.size.y / 2));
        }
        if (bottom != null){
            bottom.size = new Vector2(camWidth, bottom.size.y);
            bottom.transform.position = new Vector2(transform.position.x, transform.position.y - (camHeight / 2) - (bottom.size.y / 2));
        }
        if (left != null){
            left.size = new Vector2(left.size.x, camHeight);
            left.transform.position = new Vector2(transform.position.x - (camWidth / 2) - (left.size.x / 2), transform.position.y);
        }
        if (right != null){
            right.size = new Vector2(right.size.x, camHeight);
            right.transform.position = new Vector2(transform.position.x + (camWidth / 2) + (right.size.x / 2), transform.position.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == TagManager.Ball)
        {
            Destroy(collision.gameObject);
        }
    }
}
