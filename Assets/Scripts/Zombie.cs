﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public float movementSpeed;
    public int health;

    Transform player;
    Rigidbody2D rigid;
    bool attack = true;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag(TagManager.Player).transform;
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        Vector2 movement;
        if (attack)
        {
            var radAngle = Mathf.Atan2(player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x);
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, radAngle / (Mathf.PI / 180)));
            movement = new Vector2(Mathf.Cos(radAngle), Mathf.Sin(radAngle)) * movementSpeed;
        }
        else
        {
            movement = Vector2.down * movementSpeed;
        }
        rigid.MovePosition(new Vector2(transform.position.x + movement.x, transform.position.y + movement.y));
    }

    void GetHit()
    {
        --health;
        if (health <= 0)
        {
            Die();
        }
        else
        {
            //Play animation to get hit.
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetHit();
    }
}
