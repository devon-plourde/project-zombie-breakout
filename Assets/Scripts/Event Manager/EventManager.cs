﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour
{
    private Dictionary<string, UnityEvent> eventDictionary;

    private static EventManager eventManager;
    private static EventManager instance
    {
        get
        {
            if(eventManager == null)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
                if (eventManager == null)
                    Debug.LogWarning("There is no EventManager in this scene.");
                else
                    eventManager.Init();
            }

            return eventManager;
        }
    }

    void Init()
    {
        eventDictionary = new Dictionary<string, UnityEvent>();
    }

    public static void StartListening(string eventName, UnityAction action)
    {
        UnityEvent e = null;
        if (instance.eventDictionary.TryGetValue(eventName, out e))
        {
            e.AddListener(action);
        }
        else
        {
            e = new UnityEvent();
            e.AddListener(action);
            instance.eventDictionary.Add(eventName, e);
        }
    }

    public static void StopListening(string eventName, UnityAction action)
    {
        UnityEvent e = null;
        if(instance.eventDictionary.TryGetValue(eventName, out e))
        {
            e.RemoveListener(action);
        }
    }

    public static void StopListeningAll(string eventName)
    {
        UnityEvent e = null;
        if (instance.eventDictionary.TryGetValue(eventName, out e))
        {
            e.RemoveAllListeners();
        }
    }

    public static void TriggerEvent(string eventName)
    {
        UnityEvent e = null;
        if (instance.eventDictionary.TryGetValue(eventName, out  e))
        {
            e.Invoke();
        }

    }
}
