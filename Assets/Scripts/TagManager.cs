﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TagManager
{
    public const string Player = "Player";
    public const string MainCamera = "MainCamera";
    public const string Ball = "Ball";
    public const string Zombie = "Zombie";
}
